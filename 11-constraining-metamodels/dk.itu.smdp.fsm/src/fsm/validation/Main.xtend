/* (c) Andrzej Wąsowski 2014
 * This file allows running the constraint checker on an instance file as
 * a standalone Java application. Just run it as Java application in Eclipse.
 */
package fsm.validation

import fsm.FsmPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

class Main {

	def static void main(String[] args) {

		// register the file extension to be read as XMI
		Resource.Factory.Registry::INSTANCE.extensionToFactoryMap
			.put("xfsm", new XMIResourceFactoryImpl)

		// register our meta-model package
		FsmPackage.eINSTANCE.eClass()
		
		// load the file 
		val resourceSet = new ResourceSetImpl
		// change file name here to try other files
		val uri = URI::createURI("test-files/test-02.xfsm")
		val resource = resourceSet.getResource(uri, true)					/* true means follow proxies */

		// check constraints
		if (EcoreUtil.getAllProperContents(resource, false)
				.forall [ Constraints.constraint (it)])
			println ("All constraints are satisfied!")
		else println ("Some constraint is violated")
	}
}

