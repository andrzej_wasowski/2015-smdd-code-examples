/* (c) Andrzej Wąsowski 2014
 * A set of constraints for the FiniteStateMachine example, implemented in Xtend.
 * I avoid EMF integration for constraints on purpose, to keep this simple.
 * IMHO, you should always avoid the EMF integration for constraints, if you do
 * not explicitly need it.  It seems just way to complex.
 */
 
package fsm.validation

import fsm.FiniteStateMachine
import fsm.Model
import fsm.NamedElement
import fsm.State
import fsm.Transition
import org.eclipse.emf.ecore.EObject

class Constraints {

	/* Fall back for all types that are not constrained */
	def static dispatch boolean constraint(EObject it) {
		true
	}
	def static dispatch boolean constraint(FiniteStateMachine it) {

		/* your initial state is your own state */
		states.contains(initial)
	}

	def static dispatch boolean constraint(NamedElement it) {

		/* name cannot be empty for any named element */
		/* it is already not null by meta-model constraints */
		!name.isEmpty
	}
	
	def static dispatch boolean constraint(State it) {

		/* deterministic, so in each state each outgoing transition has a different label */
		/* not very efficient but good enough for a small example */
		leavingTransitions.forall [ t1 |
			leavingTransitions.forall[t2|t1 == t2 || t1.input != t2.input]
		] /* There must be a loop transition labeled idle for each state */ &&
			leavingTransitions.exists[t|t.input == "idle" && t.target == it]
	}

	def static dispatch boolean constraint(Transition it) {

		/* For each transition, target and source states are in the same state machine */
		source.machine == target.machine
	}

	def static dispatch boolean constraint(Model it) {

		/* Evaluation version: maximum 40 states in the model */
		/* No sharing of states is guaranteed by containment */
		machines.fold(0)[sum, it|sum + states.size] <= 40
	}

}
