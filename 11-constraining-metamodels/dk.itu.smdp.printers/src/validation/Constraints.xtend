/* (c) Andrzej Wąsowski 2014
 * Replace true constants (besides the very last one) with solutions 
 * to the exercises
 */
package validation

import org.eclipse.emf.ecore.EObject

class Constraints {

	// Constraints for the Printer Pool
	
//	def static dispatch constraint(T1.PrinterPool it) {
//		printerIsMandatory && 
//		faxRequiresAPrinter
//	}

	def static printerIsMandatory(T1.PrinterPool it) { // example
		(printer != null)
	} 

	def static faxRequiresAPrinter(T1.PrinterPool it) { // Ex. 4.1.1
		it?.fax == it?.fax
	} 

	def static dispatch constraint (T1.Fax it) { // Ex. 4.1.2
		true
	} 
	
	
	
	// Constraints for the T2 instances
	// Each Printer pool with a fax, must have a printer, 
	// and each printer pool with a copier must have a scanner and a printer.
	
	def static dispatch constraint (T2.PrinterPool it) { // Ex 4.2
		true
	}
	
	

	// Constraints for the instances of T3
	// PrinterPool’s minimum speed must be 300 lower than its regular speed.
	def static dispatch constraint (T3.PrinterPool it) { // Ex 4.3
		true 
	}

	// Constraints for the instances of T4
	// Every color printer has a colorPrinterHead.
	def static dispatch constraint (T4.Printer it) { // Ex 4.4
		true 
	}

	// Constraints for the instances of T5
	// A color-capable printer pool contains at least one 
	// color-capable printer.
	def static dispatch constraint (T5.PrinterPool it) { // Ex. 4.5
		true
	}
	
	// Constraints for the instances of T6
	def static dispatch constraint (T6.PrinterPool it) { 
		// If a Printer pool contains a color scanner, 
		// then it must contain a color printer.
		// task Ex. 4.6.2
		true	
		&& 
		// If a printer pool contains a color scanner, then all 
		// its printers must be color printers.
		// Ex. 4.6.1
		true
		&&
		// task Ex. 4.7
		// There is at most one color printer in any pool
		true
	}
	

	// Catch all case for dynamic dispatch resolution
	def static dispatch boolean constraint(EObject it) {
		true
	}
}
