/* (c) Andrzej Wąsowski 2014
 * A set of constraints for the FiniteStateMachine example, implemented in Xtend.
 * I avoid EMF integration for constraints on purpose, to keep this simple.
 * IMHO, you should always avoid the EMF integration for constraints, if you do
 * not explicitly need it.  It seems just way to complex.
 */
package validation

import org.eclipse.emf.ecore.EObject

class Constraints {

	// Constraints for the Printer Pool
	
	def static dispatch constraint(T1.PrinterPool it) {
		printerIsMandatory && 
		faxRequiresAPrinter
	}

	def static printerIsMandatory(T1.PrinterPool it) { // example
		(printer != null)
	} // satisfied by t1-01, violated by t1-00 and t1-02

	def static faxRequiresAPrinter(T1.PrinterPool it) { // task 1.1
		fax == null || printer != null
	} // satisfied by t1-00, t1-01, t1-03, violated by t1-02
	
	
	// Constraints for the Fax
	
	def static dispatch constraint (T1.Fax it) { // task 1.2
		pool.printer != null
	} // satisfied by t1-00, t1-01, t1-03, violated by t1-02
	
	
	
	// Constraints for the T2 instances
	// Each Printer pool with a fax, must have a printer, 
	// and each printer pool with a copier must have a scanner and a printer.
	
	def static dispatch constraint (T2.PrinterPool it) { // task 1.3
		(fax==null || printer!=null)
		&&
		( copier==null || (scanner!=null && printer!=null) )
	}
	
	

	// Constraints for the instances of T3
	// PrinterPool’s minimum speed must be 300 lower than its regular speed.
	def static dispatch constraint (T3.PrinterPool it) { // task 1.4
		minSpeed == speed - 300 
	}

	// Constraints for the instances of T4
	// Every color printer has a colorPrinterHead.
	def static dispatch constraint (T4.Printer it) { // task 1.5
		if (color) head!=null else true
		// alternatively: !color || head!=null 
	}

	// Constraints for the instances of T5
	// A color-capable printer pool contains at least one 
	// color-capable printer.
	def static dispatch constraint (T5.PrinterPool it) { // task 1.6
		if (color) printer.exists [color] else false
		// alternatively !color || printer.exists [color]
	}
	
	// Constraints for the instances of T6
	// If a Printer pool contains a color scanner, 
	// then it must contain a color printer.
	def static dispatch constraint (T6.PrinterPool it) { // task 1.7
		(scanner.forall [!color] || printer.exists [color])
		// alternatively
		// if (scanner.exists [color]) printer.exists [color]
		// else true
		&& 
		// If a printer pool contains a color scanner, then all 
		// its printers must be color printers.
		// task 1.8
		(scanner.forall [!color] || printer.forall [color])
		&&
		// There is at most one color printer in any pool
		// task 1.9
		printer.filter [color].size <= 1
	}
	

	// Catch all case for dynamic dispatch resolution
	def static dispatch boolean constraint(EObject it) {
		true
	}
}
