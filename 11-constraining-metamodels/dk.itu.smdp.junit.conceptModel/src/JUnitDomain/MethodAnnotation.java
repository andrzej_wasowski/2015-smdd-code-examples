/**
 */
package JUnitDomain;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see JUnitDomain.JUnitDomainPackage#getMethodAnnotation()
 * @model
 * @generated
 */
public interface MethodAnnotation extends JUAnnotation {

} // MethodAnnotation
