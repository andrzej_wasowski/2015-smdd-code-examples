/**
 */
package JUnitDomain;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>After</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see JUnitDomain.JUnitDomainPackage#getAfter()
 * @model
 * @generated
 */
public interface After extends MethodAnnotation {
} // After
