/**
 */
package JUnitDomain;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see JUnitDomain.JUnitDomainPackage#getClassAnnotation()
 * @model
 * @generated
 */
public interface ClassAnnotation extends JUAnnotation {
} // ClassAnnotation
