/**
 */
package JUnitDomain;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Setup</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see JUnitDomain.JUnitDomainPackage#getSetup()
 * @model
 * @generated
 */
public interface Setup extends EObject {
} // Setup
