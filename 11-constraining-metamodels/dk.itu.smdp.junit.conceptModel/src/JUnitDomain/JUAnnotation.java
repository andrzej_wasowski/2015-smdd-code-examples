/**
 */
package JUnitDomain;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JU Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see JUnitDomain.JUnitDomainPackage#getJUAnnotation()
 * @model
 * @generated
 */
public interface JUAnnotation extends EObject {
} // JUAnnotation
