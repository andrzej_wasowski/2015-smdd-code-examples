package dk.itu.smdp.junit.asrt.serializer;

import asrt.AsrtPackage;
import asrt.AssertMethod;
import asrt.BOp;
import asrt.BOpMethod;
import asrt.FunCall;
import asrt.Id;
import asrt.Model;
import asrt.NULL;
import asrt.Parameter;
import asrt.UOp;
import com.google.inject.Inject;
import com.google.inject.Provider;
import dk.itu.smdp.junit.asrt.services.ExternalDSLGrammarAccess;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public abstract class AbstractExternalDSLSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private ExternalDSLGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == AsrtPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case AsrtPackage.ASSERT_METHOD:
				if(context == grammarAccess.getAssertMethodRule()) {
					sequence_AssertMethod(context, (AssertMethod) semanticObject); 
					return; 
				}
				else break;
			case AsrtPackage.BOP:
				if(context == grammarAccess.getAdditionRule() ||
				   context == grammarAccess.getAdditionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getBOpMethodRule() ||
				   context == grammarAccess.getBOpMethodAccess().getBOpMethodLexprAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getConjunctionRule() ||
				   context == grammarAccess.getConjunctionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getExpRule() ||
				   context == grammarAccess.getExpAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getMultiplRule() ||
				   context == grammarAccess.getMultiplAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getPrimaryRule()) {
					sequence_Addition_Comparison_Conjunction_Exp_Multipl(context, (BOp) semanticObject); 
					return; 
				}
				else break;
			case AsrtPackage.BOP_METHOD:
				if(context == grammarAccess.getAdditionRule() ||
				   context == grammarAccess.getAdditionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getBOpMethodRule() ||
				   context == grammarAccess.getBOpMethodAccess().getBOpMethodLexprAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getConjunctionRule() ||
				   context == grammarAccess.getConjunctionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getExpRule() ||
				   context == grammarAccess.getExpAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getMultiplRule() ||
				   context == grammarAccess.getMultiplAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getPrimaryRule()) {
					sequence_BOpMethod(context, (BOpMethod) semanticObject); 
					return; 
				}
				else break;
			case AsrtPackage.FUN_CALL:
				if(context == grammarAccess.getAdditionRule() ||
				   context == grammarAccess.getAdditionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getBOpMethodRule() ||
				   context == grammarAccess.getBOpMethodAccess().getBOpMethodLexprAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getConjunctionRule() ||
				   context == grammarAccess.getConjunctionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getExpRule() ||
				   context == grammarAccess.getExpAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getFunCallRule() ||
				   context == grammarAccess.getMultiplRule() ||
				   context == grammarAccess.getMultiplAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getPrimaryRule()) {
					sequence_FunCall(context, (FunCall) semanticObject); 
					return; 
				}
				else break;
			case AsrtPackage.ID:
				if(context == grammarAccess.getAdditionRule() ||
				   context == grammarAccess.getAdditionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getBOpMethodRule() ||
				   context == grammarAccess.getBOpMethodAccess().getBOpMethodLexprAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getConjunctionRule() ||
				   context == grammarAccess.getConjunctionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getExpRule() ||
				   context == grammarAccess.getExpAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getMultiplRule() ||
				   context == grammarAccess.getMultiplAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getPrimaryRule()) {
					sequence_Primary(context, (Id) semanticObject); 
					return; 
				}
				else break;
			case AsrtPackage.MODEL:
				if(context == grammarAccess.getModelRule()) {
					sequence_Model(context, (Model) semanticObject); 
					return; 
				}
				else break;
			case AsrtPackage.NULL:
				if(context == grammarAccess.getAdditionRule() ||
				   context == grammarAccess.getAdditionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getBOpMethodRule() ||
				   context == grammarAccess.getBOpMethodAccess().getBOpMethodLexprAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getConjunctionRule() ||
				   context == grammarAccess.getConjunctionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getExpRule() ||
				   context == grammarAccess.getExpAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getMultiplRule() ||
				   context == grammarAccess.getMultiplAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getPrimaryRule()) {
					sequence_Primary(context, (NULL) semanticObject); 
					return; 
				}
				else break;
			case AsrtPackage.PARAMETER:
				if(context == grammarAccess.getParameterRule()) {
					sequence_Parameter(context, (Parameter) semanticObject); 
					return; 
				}
				else break;
			case AsrtPackage.UOP:
				if(context == grammarAccess.getAdditionRule() ||
				   context == grammarAccess.getAdditionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getBOpMethodRule() ||
				   context == grammarAccess.getBOpMethodAccess().getBOpMethodLexprAction_1_0() ||
				   context == grammarAccess.getComparisonRule() ||
				   context == grammarAccess.getComparisonAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getConjunctionRule() ||
				   context == grammarAccess.getConjunctionAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getExpRule() ||
				   context == grammarAccess.getExpAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getMultiplRule() ||
				   context == grammarAccess.getMultiplAccess().getBOpLexprAction_1_0() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getUOpRule()) {
					sequence_UOp(context, (UOp) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (
	 *         (lexpr=Addition_BOp_1_0 (operator='+' | operator='-') rexpr=Multipl) | 
	 *         (lexpr=Multipl_BOp_1_0 operator='*' rexpr=BOpMethod) | 
	 *         (
	 *             lexpr=Comparison_BOp_1_0 
	 *             (
	 *                 operator='==' | 
	 *                 operator='!=' | 
	 *                 operator='<=' | 
	 *                 operator='<' | 
	 *                 operator='>=' | 
	 *                 operator='>'
	 *             ) 
	 *             rexpr=Addition
	 *         ) | 
	 *         (lexpr=Conjunction_BOp_1_0 operator='&&' rexpr=Comparison) | 
	 *         (lexpr=Exp_BOp_1_0 operator='||' rexpr=Conjunction)
	 *     )
	 */
	protected void sequence_Addition_Comparison_Conjunction_Exp_Multipl(EObject context, BOp semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID params+=Parameter params+=Parameter* bodyExpr=Exp)
	 */
	protected void sequence_AssertMethod(EObject context, AssertMethod semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (lexpr=BOpMethod_BOpMethod_1_0 operator=ID rexpr=Exp)
	 */
	protected void sequence_BOpMethod(EObject context, BOpMethod semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AsrtPackage.Literals.BOP__OPERATOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AsrtPackage.Literals.BOP__OPERATOR));
			if(transientValues.isValueTransient(semanticObject, AsrtPackage.Literals.BOP__LEXPR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AsrtPackage.Literals.BOP__LEXPR));
			if(transientValues.isValueTransient(semanticObject, AsrtPackage.Literals.BOP__REXPR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AsrtPackage.Literals.BOP__REXPR));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBOpMethodAccess().getBOpMethodLexprAction_1_0(), semanticObject.getLexpr());
		feeder.accept(grammarAccess.getBOpMethodAccess().getOperatorIDTerminalRuleCall_1_2_0(), semanticObject.getOperator());
		feeder.accept(grammarAccess.getBOpMethodAccess().getRexprExpParserRuleCall_1_4_0(), semanticObject.getRexpr());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID (arg+=Exp arg+=Exp*)?)
	 */
	protected void sequence_FunCall(EObject context, FunCall semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     assertMethods+=AssertMethod*
	 */
	protected void sequence_Model(EObject context, Model semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (type=SimpleTypeEnum name=ID)
	 */
	protected void sequence_Parameter(EObject context, Parameter semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AsrtPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AsrtPackage.Literals.NAMED_ELEMENT__NAME));
			if(transientValues.isValueTransient(semanticObject, AsrtPackage.Literals.PARAMETER__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AsrtPackage.Literals.PARAMETER__TYPE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getParameterAccess().getTypeSimpleTypeEnumEnumRuleCall_0_0(), semanticObject.getType());
		feeder.accept(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_Primary(EObject context, Id semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AsrtPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AsrtPackage.Literals.NAMED_ELEMENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPrimaryAccess().getNameIDTerminalRuleCall_1_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     {NULL}
	 */
	protected void sequence_Primary(EObject context, NULL semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((operator='!' expr=Primary) | (operator='-' expr=Primary))
	 */
	protected void sequence_UOp(EObject context, UOp semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
