/*
* generated by Xtext
*/
package dk.itu.smdp.junit.asrt.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class ExternalDSLAntlrTokenFileProvider implements IAntlrTokenFileProvider {
	
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
    	return classLoader.getResourceAsStream("dk/itu/smdp/junit/asrt/parser/antlr/internal/InternalExternalDSL.tokens");
	}
}
