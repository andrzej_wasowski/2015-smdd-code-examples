/**
 */
package asrt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Const</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see asrt.AsrtPackage#getConst()
 * @model abstract="true"
 * @generated
 */
public interface Const extends Exp {
} // Const
