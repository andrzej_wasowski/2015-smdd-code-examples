/**
 */
package asrt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BOp Method</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see asrt.AsrtPackage#getBOpMethod()
 * @model
 * @generated
 */
public interface BOpMethod extends BOp {
} // BOpMethod
