/**
 */
package asrt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see asrt.AsrtPackage#getExp()
 * @model abstract="true"
 * @generated
 */
public interface Exp extends EObject {
} // Exp
