/**
 */
package asrt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NULL</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see asrt.AsrtPackage#getNULL()
 * @model
 * @generated
 */
public interface NULL extends Const {
} // NULL
