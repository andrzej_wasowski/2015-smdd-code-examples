/**
 */
package asrt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Id</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see asrt.AsrtPackage#getId()
 * @model
 * @generated
 */
public interface Id extends NamedElement, Exp {
} // Id
