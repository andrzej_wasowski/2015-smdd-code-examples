package dk.itu.smdp.fsm.transformations;

import com.google.common.base.Objects;
import fsm.FiniteStateMachine;
import fsm.FsmFactory;
import fsm.State;
import fsm.Transition;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class AddIdleLoops {
  public static boolean hasIdle(final State it) {
    EList<Transition> _leavingTransitions = it.getLeavingTransitions();
    final Function1<Transition,Boolean> _function = new Function1<Transition,Boolean>() {
      public Boolean apply(final Transition it) {
        String _input = it.getInput();
        boolean _equals = Objects.equal(_input, "idle");
        return Boolean.valueOf(_equals);
      }
    };
    boolean _exists = IterableExtensions.<Transition>exists(_leavingTransitions, _function);
    return _exists;
  }
  
  public static void addIdle(final State s) {
    EList<Transition> _leavingTransitions = s.getLeavingTransitions();
    Transition _createTransition = FsmFactory.eINSTANCE.createTransition();
    final Procedure1<Transition> _function = new Procedure1<Transition>() {
      public void apply(final Transition it) {
        it.setInput("idle");
        it.setOutput(null);
        it.setTarget(s);
      }
    };
    Transition _doubleArrow = ObjectExtensions.<Transition>operator_doubleArrow(_createTransition, _function);
    _leavingTransitions.add(_doubleArrow);
  }
  
  public static void run(final FiniteStateMachine it) {
    EList<State> _states = it.getStates();
    final Procedure1<State> _function = new Procedure1<State>() {
      public void apply(final State it) {
        boolean _hasIdle = AddIdleLoops.hasIdle(it);
        boolean _not = (!_hasIdle);
        if (_not) {
          AddIdleLoops.addIdle(it);
        }
      }
    };
    IterableExtensions.<State>forEach(_states, _function);
  }
}
