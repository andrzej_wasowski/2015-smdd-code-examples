package dk.itu.smdp.fsm.transformations;

import com.google.inject.Injector;
import dk.itu.smdp.fsm.ExternalDSLStandaloneSetup;
import dk.itu.smdp.fsm.transformations.AddIdleLoops;
import fsm.FiniteStateMachine;
import fsm.FsmPackage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class Main {
  private final static String instanceFileName = "test-files/CoffeeMachine.fsm";
  
  public static void main(final String[] args) {
    try {
      FsmPackage.eINSTANCE.eClass();
      ExternalDSLStandaloneSetup _externalDSLStandaloneSetup = new ExternalDSLStandaloneSetup();
      final Injector injector = _externalDSLStandaloneSetup.createInjectorAndDoEMFRegistration();
      final XtextResourceSet resourceSet = injector.<XtextResourceSet>getInstance(XtextResourceSet.class);
      resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.valueOf(true));
      final URI uri = URI.createURI(Main.instanceFileName);
      Resource resource = resourceSet.getResource(uri, true);
      EList<EObject> _contents = resource.getContents();
      EObject _get = _contents.get(0);
      final FiniteStateMachine m = ((FiniteStateMachine) _get);
      AddIdleLoops.run(m);
      final URI outputURI = URI.createFileURI("src-gen/test-output.fsm");
      ResourceSet _resourceSet = resource.getResourceSet();
      URIConverter _uRIConverter = _resourceSet.getURIConverter();
      URI _normalize = _uRIConverter.normalize(outputURI);
      resource.setURI(_normalize);
      resource.save(null);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
