package dk.itu.smdp.xml.internalDSL

import java.util.List
import java.util.Map
import xml.Attribute
import xml.Tag
import xml.XmlFactory

class Model {

	// 50-60 lines of code is expected 
	
	def static Tag XML ((Tag)=>void addContents) {
		val root = XmlFactory.eINSTANCE.createTag => [name = "xml"]
		addContents.apply(root)
		return root
	}

	// contents optional
	def static Tag XML() {
		XML([t|])
	}

	// tags nested in tags
	def static Tag tag(Tag parent, String name, Map<String, String> attrs, (Tag)=>void addContents) {
		val tag = XmlFactory.eINSTANCE.createTag => [it.name = name]
		addContents.apply(tag.addAttributes(attrs))
		parent.nestedTags.add(tag)
		return parent
	}

	// many attributes, content optional
	def static Tag tag(Tag parent, String name, Map<String, String> attrs) {
		tag(parent, name, attrs, [])
	}

	// Single attribute, content
	def static Tag tag(Tag root, String name, Pair<String, String> attr, (Tag)=>void addContents) {
		tag(root, name, #{attr}, addContents)
	}

	// Single attribute, no content
	def static Tag tag(Tag root, String name, Pair<String, String> attr) {
		tag(root, name, #{attr}, [])
	}

	//  No attributes, content
	def static Tag tag(Tag root, String name, (Tag)=>void addContents) {
		tag(root, name, #{}, addContents)
	}
	
	//  No attributes, no content
	def static Tag tag(Tag root, String name) {
		tag(root, name, #{}, [])
	}
	
	// helpers
	def private static Tag addAttributes(Tag tag, Map<String, String> attrs) {

		attrs.forEach [ p1, p2 |
			val Attribute a = XmlFactory.eINSTANCE.createAttribute => [name = p1; value = p2]
			val xml_attrs = tag.attrs as List<Attribute>
			xml_attrs.add(a)
		]
		return tag
	}

	// Serialization
	def static StringBuilder toStringBuilder(Attribute attr) {

		new StringBuilder(" ").append(attr.name).append("=\"").append(attr.value).append('"')

	}

	def static StringBuilder toStringBuilder1(List<Attribute> attrs) {

		attrs.fold(new StringBuilder)[StringBuilder sb, Attribute a|sb.append(a.toStringBuilder)]
	}

	def static StringBuilder toStringBuilder(Tag tag) {

		var StringBuilder retval = new StringBuilder('<').append(tag.name)
		retval.append(toStringBuilder1(tag.attrs))
		if (tag.nestedTags.isEmpty)
			retval.append("/>\n")
		else {
			retval.append(">\n");
			retval.append((tag.nestedTags as List<Tag>).toStringBuilder2)
			retval.append("</").append(tag.name).append(">\n")
		}

		return retval
	}

	def static StringBuilder toStringBuilder2(List<Tag> tags) {

		tags.fold(new StringBuilder)[StringBuilder sb, Tag t|sb.append(t.toStringBuilder)]
	}

	def static String serialize(Tag root) {
		root.toStringBuilder.toString
	}

}
