import xml.Tag

class XmlTest00 extends dk.itu.smdp.xml.internalDSL.Model {

	def static void main(String[] args) {

		val Tag mydoc = 
		
		XML [
			tag ("course", #{"name" -> "SMDP", "students" -> "89", "day" -> "Monday"}) []
			tag ("course", #{"name" -> "SPLC", "students" -> "21"}) []
			tag ("course", #{"name" -> "SASP", "students" -> "10", "day" -> "Tuesday"})
			tag ("course", #{"name" -> "SPLS", "students" -> "3"}) 
		]

	val emptydoc = XML

	print(mydoc.serialize)print(emptydoc.serialize)}

}
