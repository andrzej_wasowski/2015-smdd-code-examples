class XmlTest03 extends dk.itu.smdp.xml.internalDSL.Model {

	def static void main(String[] args) {

		val mydoc = 

		// because this is an embedded DSL we can play with templating, by
		// mixing Xtend and our internal-DSL for XML
				
		XML [ 
				for (i : 1..10)
					tag ("number", "value" -> i.toString)
						
		]

		print(mydoc.serialize)
	}

}
