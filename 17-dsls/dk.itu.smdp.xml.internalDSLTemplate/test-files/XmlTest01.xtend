class XmlTest01 extends dk.itu.smdp.xml.internalDSL.Model {

	def static void main(String[] args) {

		val mydoc = XML[
			
			tag ("program", #{"name" -> "SDT"}) [
				tag("course", #{"name" -> "SMDP", "students" -> "89", "day" -> "Monday"})
				[
					tag ("lecture01", "title" -> "Introduction")
					tag ("lecture02", "title" -> "Programming Language Implementation Primer")
				]
				tag("course", #{"name" -> "SPLC", "students" -> "21"}) 
				tag("course", #{"name" -> "SASP", "students" -> "10", "day" -> "Tuesday"})
				tag("course", #{"name" -> "SPLS", "students" -> "3"})
			] 
		]

		print(mydoc.serialize)
	}

}
