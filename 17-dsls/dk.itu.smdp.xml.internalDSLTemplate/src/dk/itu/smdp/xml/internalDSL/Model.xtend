package dk.itu.smdp.xml.internalDSL

import java.util.List
import java.util.Map
import xml.Attribute
import xml.Tag
import xml.XmlFactory

class Model {

	// The main two function types below (XML and tag) are pre-filled to make the start easier.
	// You need to fill in this functions and add a few more, as you see feet.
	// Code for serialization is already provided in the bottom of this file.

	// 50-60 lines of code are expected to be added to this file in total to solve this exercise
	
	// Test cases are in test-files.  We recommend using these in order 
	// (first implement enough to handle 00, then 01 and so on) 
	
	def static Tag XML ((Tag)=>void addContents) {
		// TODO 
	}


	def static Tag tag(Tag parent, String name, Map<String, String> attrs, (Tag)=>void addContents) {

		// TODO 
	}


	// Serialization
	def static StringBuilder toStringBuilder(Attribute attr) {

		new StringBuilder(" ").append(attr.name).append("=\"").append(attr.value).append('"')

	}

	def static StringBuilder toStringBuilder1(List<Attribute> attrs) {

		attrs.fold(new StringBuilder)[StringBuilder sb, Attribute a|sb.append(a.toStringBuilder)]
	}

	def static StringBuilder toStringBuilder(Tag tag) {

		var StringBuilder retval = new StringBuilder('<').append(tag.name)
		retval.append(toStringBuilder1(tag.attrs))
		if (tag.nestedTags.isEmpty)
			retval.append("/>\n")
		else {
			retval.append(">\n");
			retval.append((tag.nestedTags as List<Tag>).toStringBuilder2)
			retval.append("</").append(tag.name).append(">\n")
		}

		return retval
	}

	def static StringBuilder toStringBuilder2(List<Tag> tags) {

		tags.fold(new StringBuilder)[StringBuilder sb, Tag t|sb.append(t.toStringBuilder)]
	}

	def static String serialize(Tag root) {
		root.toStringBuilder.toString
	}

}
