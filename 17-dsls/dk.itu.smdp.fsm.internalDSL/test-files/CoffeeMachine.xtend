/* (c) Andrzej Wąsowski 2014
 * An example of a state machine in the FSM language (implemented as internal DSL)
 * Only one machine per model is considered for simplicity.
 */

import fsm.internalDSL.Model

/*
 * This is an example of possible syntax that I prototyped as free text.
 * You can compare it with the result below the comment
 * 
 * Model CoffeeMachine 
 * 
 * Machine Coffee {
 * 	
 * 	initial [ if (coin) then "what drink do you want?" => choice ]
 * 
 * choice [ if (tea) 	 then "serving tea" 			   => makeTea
 * 			if (coffee)  then "serving coffee" 			   => makeCoffee 
 * 			if (timeout) then "coin returned; insert coin" => initial
 * ]
 * 
 * makeCofee [ if (5s) then "coffee served. Enjoy!" => initial ]
 * makeTea   [ if (5s) then "tea served. Enjoy!" 	=> initial ] 
 * 
 * }
 */
	 
class CoffeeMachine extends Model { 

	val static m = 
	
	machine ("coffeeMachine") [
		
		initial ("initial") [
			on input ("coin") 		output  ("what drink do you want?")		and go to ("selection")
			on input ("break") 		output  ("machine is broken")	 		and go to ("broken")
		]

		state ("selection", [
			on input ("tea") 		output ("serving tea") 					and go to ("making tea")
			on input ("coffee")		output ("serving coffee")				and go to ("making coffee")
			on input ("timeout")	output ("coin returned; insert coin")	and go to ("initial")
			on input ("break")		output ("machine is broken!")			and go to ("broken")
		])

		state ("making coffee")  [
			on input ("done") 		output ("coffee served. Enjoy!") 		and go to ("initial")
			on input ("break")		output ("machine is broken!")			and go to ("broken")
		]

		state ("making tea") [
			on input ("done") 		output ("tea served. Enjoy!") 			and go to ("initial")
			on input ("break")		output ("machine is broken!")			and go to ("broken")
		]
		
		state ("broken") [ go on and on and on and on ] 
	]

	// state 'broken' is not declared 
	// (mentioned states are automatically created in this implementation)
		
	
	/* this is not a part of the domain specific model. 
	 * I just put the main function here to avoid creating an
	 * additional file for it
	 */
	def static void main(String[] args) { m.run }
} 
