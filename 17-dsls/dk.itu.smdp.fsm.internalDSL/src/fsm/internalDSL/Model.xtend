/* (c) Andrzej Wąsowski 2014
 * A simple implementation of the FSM language as an internal DSL in Xtend.
 * Only one machine per model is considered for simplicity. 
 */
package fsm.internalDSL

import fsm.FiniteStateMachine
import fsm.FsmFactory
import fsm.State
import fsm.Transition 
import org.eclipse.xtext.xbase.lib.Pair
import java.util.Scanner
import java.util.List

class IncompleteTransition {
	@Property var String input
	@Property var String output
	@Property var String source
	

//	def FiniteStateMachine installTransition (FiniteStateMachine m, String targetName ) {
//
//		val t = FsmFactory.eINSTANCE.createTransition => [ 
//			input = this.input
//			output = this.output
//			source = m.states.findFirst [it.name == this.source]
//			target = Model.addState (m, targetName)
//		]
//		t.source.leavingTransitions.add (t)
//		
//		// prevent acciddental transfer of values if user omits a clause
//		input = null
//		output = null
//		return m
//	}
}



class Model {

	// ** faking some keywords ** 
	//
	// we make keywords into functions, so that they can be placed in lambda expressions
	//
	// The parameter makes them extension methods (which then results in the same 
	// syntax highlighting as for the other keywords using extension methods)
	//
	// I can remove the parameter (so it is void) and then the syntax highlighting changes
	// this can be sometimes useful
	//
	// With some more trickery, if static call can be avoided, then also the italics (in my
	// theme) would go from the function name
	
	def static on  (Pair<FiniteStateMachine,IncompleteTransition> it) {}
	def static and (Pair<FiniteStateMachine,IncompleteTransition> it) {}
	def static go  (Pair<FiniteStateMachine,IncompleteTransition> it) {}
	
	/* Finite State Machine */
	
	def static machine (String name, (FiniteStateMachine)=>void addStates) 
	{
		val m = FsmFactory.eINSTANCE.createFiniteStateMachine ()
		addStates.apply (m)
		return m 
	}

	// machine body is made optional by this overloading
	def static machine (String name) { machine (name, []) }



	/* State */

	// implementation of state block
	def static void state (FiniteStateMachine m, String name, 
					(Pair<FiniteStateMachine,IncompleteTransition>)=>void addTransitions)
	{
		val t = new IncompleteTransition => [ source = name ]
		m.addState (name)
		addTransitions.apply(m -> t)
	}
	
	// state's body is made optional using this overloading
	def static void state (FiniteStateMachine m, String name)
	{ state (m,name,[]) } 

	def static State addState (FiniteStateMachine m, String name) 
	{ 
		val s = m.states.findFirst [s | s.name == name] ?:
			(FsmFactory.eINSTANCE.createState => [ s | s.name = name ])
		m.states.add (s)
		return s
	}



	/* Initial state */

	def static void initial (FiniteStateMachine m, String name, 
					(Pair<FiniteStateMachine,IncompleteTransition>)=>void addTransitions)
	{
		state (m, name) [ 
			// record the initial state before proceeding to transitions
			key.initial = key.states.findFirst [s | s.name == name] 
			addTransitions.apply (it) 
		]
	}
	
	// initial state's body is made optional using this overloading
	def static void initial (FiniteStateMachine m, String name)
	{ initial (m,name,[])}



	/* Transitions */

	def static void input (Pair<FiniteStateMachine,IncompleteTransition> it, String input)
	{ value.input = input }
	
	def static void output  (Pair<FiniteStateMachine,IncompleteTransition> it, String output)
	{ value.output = output }
		
	def static void to (Pair<FiniteStateMachine,IncompleteTransition> it, String target)
	{ key.installTransition (value,target) }

	def static void installTransition (FiniteStateMachine m, IncompleteTransition t, String targetName ) {

		FsmFactory.eINSTANCE.createTransition => [
			input = t.input
			output = t.output
			// this also sets the other end of reference, connecting the transition (thanks to EMF's EOpposite)
			source = m.states.findFirst [it.name == t.source] 
			target = m.addState (targetName)
		]

		// prevent accidental transfer of values if user omits a clause
		t.input = null
		t.output = null
	}


	/* This is the actual interpreter */
	
	def static run (FiniteStateMachine it) {
		
		/* This is a good moment to check constraints
		// As internal DSLs are weak in syntax/type checking usually
		// I do not check constraints for simplicity, but feel free
		// to integrate this in as an exercise
		// (note that some constraints of previous lectures should
		//  be deactivated, as they do not make much sense for this example
		// -- they were added to illustrate constraint writing, not so much
		// to improve FSM) */
		
		var current_state = initial
		var scanner = new Scanner(System.in);
		
		while (true) {
			val List<Transition> ts = current_state.leavingTransitions
			
			print ("["+ current_state.name + "] What is the next event? available:")
			ts.forEach [print (" <" + input + ">")]
			print ("?")

			val input = scanner.nextLine
			val t = ts.findFirst [t | t.input == input] 
			
			if (t != null) {
				println ("machine says: " + t.output)
				current_state = t.target
			} else println ("invalid input")
		}
	}
}