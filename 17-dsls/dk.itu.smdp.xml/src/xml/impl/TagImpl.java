/**
 */
package xml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import xml.Attribute;
import xml.Tag;
import xml.XmlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tag</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link xml.impl.TagImpl#getNestedTags <em>Nested Tags</em>}</li>
 *   <li>{@link xml.impl.TagImpl#getAttrs <em>Attrs</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TagImpl extends NamedElementImpl implements Tag {
	/**
	 * The cached value of the '{@link #getNestedTags() <em>Nested Tags</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestedTags()
	 * @generated
	 * @ordered
	 */
	protected EList<Tag> nestedTags;

	/**
	 * The cached value of the '{@link #getAttrs() <em>Attrs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttrs()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> attrs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TagImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XmlPackage.Literals.TAG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Tag> getNestedTags() {
		if (nestedTags == null) {
			nestedTags = new EObjectContainmentEList<Tag>(Tag.class, this, XmlPackage.TAG__NESTED_TAGS);
		}
		return nestedTags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attribute> getAttrs() {
		if (attrs == null) {
			attrs = new EObjectContainmentEList<Attribute>(Attribute.class, this, XmlPackage.TAG__ATTRS);
		}
		return attrs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XmlPackage.TAG__NESTED_TAGS:
				return ((InternalEList<?>)getNestedTags()).basicRemove(otherEnd, msgs);
			case XmlPackage.TAG__ATTRS:
				return ((InternalEList<?>)getAttrs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case XmlPackage.TAG__NESTED_TAGS:
				return getNestedTags();
			case XmlPackage.TAG__ATTRS:
				return getAttrs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case XmlPackage.TAG__NESTED_TAGS:
				getNestedTags().clear();
				getNestedTags().addAll((Collection<? extends Tag>)newValue);
				return;
			case XmlPackage.TAG__ATTRS:
				getAttrs().clear();
				getAttrs().addAll((Collection<? extends Attribute>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case XmlPackage.TAG__NESTED_TAGS:
				getNestedTags().clear();
				return;
			case XmlPackage.TAG__ATTRS:
				getAttrs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case XmlPackage.TAG__NESTED_TAGS:
				return nestedTags != null && !nestedTags.isEmpty();
			case XmlPackage.TAG__ATTRS:
				return attrs != null && !attrs.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TagImpl
