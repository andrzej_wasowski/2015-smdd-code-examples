/**
 */
package xml;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link xml.Tag#getNestedTags <em>Nested Tags</em>}</li>
 *   <li>{@link xml.Tag#getAttrs <em>Attrs</em>}</li>
 * </ul>
 * </p>
 *
 * @see xml.XmlPackage#getTag()
 * @model
 * @generated
 */
public interface Tag extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Nested Tags</b></em>' containment reference list.
	 * The list contents are of type {@link xml.Tag}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nested Tags</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nested Tags</em>' containment reference list.
	 * @see xml.XmlPackage#getTag_NestedTags()
	 * @model containment="true"
	 * @generated
	 */
	EList<Tag> getNestedTags();

	/**
	 * Returns the value of the '<em><b>Attrs</b></em>' containment reference list.
	 * The list contents are of type {@link xml.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attrs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attrs</em>' containment reference list.
	 * @see xml.XmlPackage#getTag_Attrs()
	 * @model containment="true"
	 * @generated
	 */
	EList<Attribute> getAttrs();

} // Tag
