/*
 * generated by Xtext
 */
package dk.itu.smdp.fsm;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class ExternalDSLRuntimeModule extends dk.itu.smdp.fsm.AbstractExternalDSLRuntimeModule {

}
