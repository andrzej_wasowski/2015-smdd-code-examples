package dk.itu.smdp.fsm.externalDSL;

import dk.itu.smdp.fsm.ExternalDSLStandaloneSetup;
import fsm.FiniteStateMachine;
import fsm.FsmPackage;
import fsm.internalDSL.Model;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

@SuppressWarnings("all")
public class FSMRunner {
  private final static String instanceFileName = "test-files/CoffeeMachine.fsm";
  
  public static void main(final String[] args) {
    FsmPackage.eINSTANCE.eClass();
    ExternalDSLStandaloneSetup _externalDSLStandaloneSetup = new ExternalDSLStandaloneSetup();
    _externalDSLStandaloneSetup.createInjectorAndDoEMFRegistration();
    final URI uri = URI.createURI(FSMRunner.instanceFileName);
    ResourceSetImpl _resourceSetImpl = new ResourceSetImpl();
    Resource resource = _resourceSetImpl.getResource(uri, true);
    EList<EObject> _contents = resource.getContents();
    EObject _get = _contents.get(0);
    final FiniteStateMachine m = ((FiniteStateMachine) _get);
    Model.run(m);
  }
}
