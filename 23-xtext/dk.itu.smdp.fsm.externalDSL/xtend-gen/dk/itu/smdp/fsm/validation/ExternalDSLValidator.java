/**
 * generated by Xtext
 */
package dk.itu.smdp.fsm.validation;

import dk.itu.smdp.fsm.validation.AbstractExternalDSLValidator;

/**
 * Custom validation rules.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
@SuppressWarnings("all")
public class ExternalDSLValidator extends AbstractExternalDSLValidator {
}
