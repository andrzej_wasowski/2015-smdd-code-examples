package dk.itu.smdp.fsm.generator;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fsm.FiniteStateMachine;
import fsm.State;
import fsm.Transition;
import java.io.File;
import java.util.Collections;
import java.util.List;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class ExternalDSLGenerator implements IGenerator {
  public static CharSequence compileToJava(final FiniteStateMachine it) {
    CharSequence _xblockexpression = null;
    {
      int i = (-1);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("import java.util.Scanner;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("class FSM");
      String _name = it.getName();
      String _firstUpper = StringExtensions.toFirstUpper(_name);
      _builder.append(_firstUpper, "");
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      {
        EList<State> _states = it.getStates();
        for(final State state : _states) {
          _builder.append("\t");
          _builder.append("static final int ");
          String _name_1 = state.getName();
          String _upperCase = _name_1.toUpperCase();
          _builder.append(_upperCase, "\t");
          _builder.append(" = ");
          int _i = i = (i + 1);
          _builder.append(_i, "\t");
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t");
      _builder.append("static int current;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("static final String[] stateNames = { ");
      _builder.newLine();
      _builder.append("\t\t");
      {
        EList<State> _states_1 = it.getStates();
        for(final State state_1 : _states_1) {
          _builder.append("\"");
          String _name_2 = state_1.getName();
          _builder.append(_name_2, "\t\t");
          _builder.append("\",");
        }
      }
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("static final String[] availableInputs = {");
      _builder.newLine();
      {
        EList<State> _states_2 = it.getStates();
        for(final State state_2 : _states_2) {
          _builder.append("\t\t");
          _builder.append("\"");
          {
            EList<Transition> _leavingTransitions = state_2.getLeavingTransitions();
            for(final Transition t : _leavingTransitions) {
              _builder.append("<");
              String _input = t.getInput();
              _builder.append(_input, "\t\t");
              _builder.append(">");
            }
          }
          _builder.append("\",");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t");
      _builder.append("};");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("public static void main (String[] args) {");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("@SuppressWarnings(value = { \"resource\" })");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Scanner scanner = new Scanner(System.in);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("current = ");
      State _initial = it.getInitial();
      String _name_3 = _initial.getName();
      String _upperCase_1 = _name_3.toUpperCase();
      _builder.append(_upperCase_1, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("while (true) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("System.out.print (\"[\" + stateNames[current] + \"] \");");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("System.out.print (\"What is the next event? available: \" + availableInputs[current]);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("System.out.print (\"?\");");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("String input = scanner.nextLine();");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("switch (current) {");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      {
        EList<State> _states_3 = it.getStates();
        for(final State state_3 : _states_3) {
          _builder.append("\t\t");
          _builder.append("case ");
          String _name_4 = state_3.getName();
          String _upperCase_2 = _name_4.toUpperCase();
          _builder.append(_upperCase_2, "\t\t");
          _builder.append(":");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t");
          _builder.append("\t");
          _builder.append("switch (input) {");
          _builder.newLine();
          {
            EList<Transition> _leavingTransitions_1 = state_3.getLeavingTransitions();
            for(final Transition t_1 : _leavingTransitions_1) {
              _builder.append("\t\t");
              _builder.append("\t");
              _builder.append("case \"");
              String _input_1 = t_1.getInput();
              _builder.append(_input_1, "\t\t\t");
              _builder.append("\":");
              _builder.newLineIfNotEmpty();
              _builder.append("\t\t");
              _builder.append("\t");
              _builder.append("\t");
              _builder.append("System.out.println (\"machine says: ");
              String _output = t_1.getOutput();
              _builder.append(_output, "\t\t\t\t");
              _builder.append("\");");
              _builder.newLineIfNotEmpty();
              _builder.append("\t\t");
              _builder.append("\t");
              _builder.append("\t");
              _builder.append("current = ");
              State _target = t_1.getTarget();
              String _name_5 = _target.getName();
              String _upperCase_3 = _name_5.toUpperCase();
              _builder.append(_upperCase_3, "\t\t\t\t");
              _builder.append(";");
              _builder.newLineIfNotEmpty();
              _builder.append("\t\t");
              _builder.append("\t");
              _builder.append("\t");
              _builder.append("break;");
              _builder.newLine();
            }
          }
          _builder.append("\t\t");
          _builder.append("\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t");
          _builder.append("break;");
          _builder.newLine();
        }
      }
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public static CharSequence compileToDot(final FiniteStateMachine it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("digraph \"");
    String _name = it.getName();
    _builder.append(_name, "");
    _builder.append("\" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("_init -> ");
    State _initial = it.getInitial();
    String _name_1 = _initial.getName();
    _builder.append(_name_1, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    {
      EList<State> _states = it.getStates();
      for(final State state : _states) {
        {
          EList<Transition> _leavingTransitions = state.getLeavingTransitions();
          for(final Transition t : _leavingTransitions) {
            _builder.append("\t");
            _builder.append("\"");
            String _name_2 = state.getName();
            _builder.append(_name_2, "\t");
            _builder.append("\" -> \"");
            State _target = t.getTarget();
            String _name_3 = _target.getName();
            _builder.append(_name_3, "\t");
            _builder.append("\" [label=\"");
            String _input = t.getInput();
            _builder.append(_input, "\t");
            _builder.append(" / ");
            String _output = t.getOutput();
            _builder.append(_output, "\t");
            _builder.append(" \"];");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("\t");
    State _initial_1 = it.getInitial();
    String _name_4 = _initial_1.getName();
    _builder.append(_name_4, "\t");
    _builder.append(" [shape=doublecircle];");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("_init [shape=point];");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public void doGenerate(final Resource resource, final IFileSystemAccess fsa) {
    TreeIterator<EObject> _allContents = resource.getAllContents();
    Iterable<EObject> _iterable = IteratorExtensions.<EObject>toIterable(_allContents);
    Iterable<FiniteStateMachine> _filter = Iterables.<FiniteStateMachine>filter(_iterable, FiniteStateMachine.class);
    final Procedure1<FiniteStateMachine> _function = new Procedure1<FiniteStateMachine>() {
      public void apply(final FiniteStateMachine it) {
        try {
          String _name = it.getName();
          final String fname = StringExtensions.toFirstUpper(_name);
          CharSequence _compileToJava = ExternalDSLGenerator.compileToJava(it);
          fsa.generateFile((("fsm/" + fname) + ".java"), _compileToJava);
          CharSequence _compileToDot = ExternalDSLGenerator.compileToDot(it);
          fsa.generateFile((fname + ".dot"), _compileToDot);
          URI _uRI = resource.getURI();
          final String projectName = _uRI.segment(1);
          IWorkspace _workspace = ResourcesPlugin.getWorkspace();
          IWorkspaceRoot _root = _workspace.getRoot();
          final IProject project = _root.getProject(projectName);
          IPath _location = project.getLocation();
          String _plus = (_location + "/src-gen/");
          File path = new File(_plus);
          List<String> cmd = Collections.<String>unmodifiableList(Lists.<String>newArrayList("dot", "-Tpdf", (fname + ".dot"), "-o", (fname + ".pdf")));
          Runtime _runtime = Runtime.getRuntime();
          final List<String> _converted_cmd = (List<String>)cmd;
          Process _exec = _runtime.exec(((String[])Conversions.unwrapArray(_converted_cmd, String.class)), null, path);
          _exec.wait();
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      }
    };
    IterableExtensions.<FiniteStateMachine>forEach(_filter, _function);
  }
}
