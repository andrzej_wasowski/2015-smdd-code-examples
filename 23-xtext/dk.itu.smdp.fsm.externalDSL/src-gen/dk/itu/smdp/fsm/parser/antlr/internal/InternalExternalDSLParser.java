package dk.itu.smdp.fsm.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import dk.itu.smdp.fsm.services.ExternalDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalExternalDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'machine'", "'['", "'initial'", "']'", "'state'", "'on'", "'input'", "'output'", "'and'", "'go'", "'to'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalExternalDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalExternalDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalExternalDSLParser.tokenNames; }
    public String getGrammarFileName() { return "../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g"; }



     	private ExternalDSLGrammarAccess grammarAccess;
     	
        public InternalExternalDSLParser(TokenStream input, ExternalDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "FiniteStateMachine";	
       	}
       	
       	@Override
       	protected ExternalDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleFiniteStateMachine"
    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:67:1: entryRuleFiniteStateMachine returns [EObject current=null] : iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF ;
    public final EObject entryRuleFiniteStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFiniteStateMachine = null;


        try {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:68:2: (iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:69:2: iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF
            {
             newCompositeNode(grammarAccess.getFiniteStateMachineRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFiniteStateMachine_in_entryRuleFiniteStateMachine75);
            iv_ruleFiniteStateMachine=ruleFiniteStateMachine();

            state._fsp--;

             current =iv_ruleFiniteStateMachine; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFiniteStateMachine85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFiniteStateMachine"


    // $ANTLR start "ruleFiniteStateMachine"
    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:76:1: ruleFiniteStateMachine returns [EObject current=null] : (otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )? ) ;
    public final EObject ruleFiniteStateMachine() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_states_5_0 = null;


         enterRule(); 
            
        try {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:79:28: ( (otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )? ) )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:80:1: (otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )? )
            {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:80:1: (otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )? )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:80:3: otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )?
            {
            otherlv_0=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleFiniteStateMachine122); 

                	newLeafNode(otherlv_0, grammarAccess.getFiniteStateMachineAccess().getMachineKeyword_0());
                
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:84:1: ( (lv_name_1_0= RULE_ID ) )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:85:1: (lv_name_1_0= RULE_ID )
            {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:85:1: (lv_name_1_0= RULE_ID )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:86:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleFiniteStateMachine139); 

            			newLeafNode(lv_name_1_0, grammarAccess.getFiniteStateMachineAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFiniteStateMachineRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:102:2: (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:102:4: otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']'
                    {
                    otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleFiniteStateMachine157); 

                        	newLeafNode(otherlv_2, grammarAccess.getFiniteStateMachineAccess().getLeftSquareBracketKeyword_2_0());
                        
                    otherlv_3=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleFiniteStateMachine169); 

                        	newLeafNode(otherlv_3, grammarAccess.getFiniteStateMachineAccess().getInitialKeyword_2_1());
                        
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:110:1: ( (otherlv_4= RULE_ID ) )
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:111:1: (otherlv_4= RULE_ID )
                    {
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:111:1: (otherlv_4= RULE_ID )
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:112:3: otherlv_4= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getFiniteStateMachineRule());
                    	        }
                            
                    otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleFiniteStateMachine189); 

                    		newLeafNode(otherlv_4, grammarAccess.getFiniteStateMachineAccess().getInitialStateCrossReference_2_2_0()); 
                    	

                    }


                    }

                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:123:2: ( (lv_states_5_0= ruleState ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==15) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:124:1: (lv_states_5_0= ruleState )
                    	    {
                    	    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:124:1: (lv_states_5_0= ruleState )
                    	    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:125:3: lv_states_5_0= ruleState
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFiniteStateMachineAccess().getStatesStateParserRuleCall_2_3_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleState_in_ruleFiniteStateMachine210);
                    	    lv_states_5_0=ruleState();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFiniteStateMachineRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"states",
                    	            		lv_states_5_0, 
                    	            		"State");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleFiniteStateMachine223); 

                        	newLeafNode(otherlv_6, grammarAccess.getFiniteStateMachineAccess().getRightSquareBracketKeyword_2_4());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFiniteStateMachine"


    // $ANTLR start "entryRuleState"
    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:153:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:154:2: (iv_ruleState= ruleState EOF )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:155:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleState_in_entryRuleState261);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleState271); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:162:1: ruleState returns [EObject current=null] : (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )? ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_leavingTransitions_3_0 = null;


         enterRule(); 
            
        try {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:165:28: ( (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )? ) )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:166:1: (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )? )
            {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:166:1: (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )? )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:166:3: otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )?
            {
            otherlv_0=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleState308); 

                	newLeafNode(otherlv_0, grammarAccess.getStateAccess().getStateKeyword_0());
                
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:170:1: ( (lv_name_1_0= RULE_ID ) )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:171:1: (lv_name_1_0= RULE_ID )
            {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:171:1: (lv_name_1_0= RULE_ID )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:172:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleState325); 

            			newLeafNode(lv_name_1_0, grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getStateRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:188:2: (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==12) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:188:4: otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']'
                    {
                    otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleState343); 

                        	newLeafNode(otherlv_2, grammarAccess.getStateAccess().getLeftSquareBracketKeyword_2_0());
                        
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:192:1: ( (lv_leavingTransitions_3_0= ruleTransition ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==16) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:193:1: (lv_leavingTransitions_3_0= ruleTransition )
                    	    {
                    	    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:193:1: (lv_leavingTransitions_3_0= ruleTransition )
                    	    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:194:3: lv_leavingTransitions_3_0= ruleTransition
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getStateAccess().getLeavingTransitionsTransitionParserRuleCall_2_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleTransition_in_ruleState364);
                    	    lv_leavingTransitions_3_0=ruleTransition();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getStateRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"leavingTransitions",
                    	            		lv_leavingTransitions_3_0, 
                    	            		"Transition");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleState377); 

                        	newLeafNode(otherlv_4, grammarAccess.getStateAccess().getRightSquareBracketKeyword_2_2());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:222:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:223:2: (iv_ruleTransition= ruleTransition EOF )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:224:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleTransition_in_entryRuleTransition415);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTransition425); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:231:1: ruleTransition returns [EObject current=null] : (otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_input_2_0=null;
        Token otherlv_3=null;
        Token lv_output_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;

         enterRule(); 
            
        try {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:234:28: ( (otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) ) ) )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:235:1: (otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) ) )
            {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:235:1: (otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) ) )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:235:3: otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleTransition462); 

                	newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getOnKeyword_0());
                
            otherlv_1=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleTransition474); 

                	newLeafNode(otherlv_1, grammarAccess.getTransitionAccess().getInputKeyword_1());
                
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:243:1: ( (lv_input_2_0= RULE_STRING ) )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:244:1: (lv_input_2_0= RULE_STRING )
            {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:244:1: (lv_input_2_0= RULE_STRING )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:245:3: lv_input_2_0= RULE_STRING
            {
            lv_input_2_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleTransition491); 

            			newLeafNode(lv_input_2_0, grammarAccess.getTransitionAccess().getInputSTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTransitionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"input",
                    		lv_input_2_0, 
                    		"STRING");
            	    

            }


            }

            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:261:2: (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:261:4: otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) )
                    {
                    otherlv_3=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleTransition509); 

                        	newLeafNode(otherlv_3, grammarAccess.getTransitionAccess().getOutputKeyword_3_0());
                        
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:265:1: ( (lv_output_4_0= RULE_STRING ) )
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:266:1: (lv_output_4_0= RULE_STRING )
                    {
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:266:1: (lv_output_4_0= RULE_STRING )
                    // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:267:3: lv_output_4_0= RULE_STRING
                    {
                    lv_output_4_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleTransition526); 

                    			newLeafNode(lv_output_4_0, grammarAccess.getTransitionAccess().getOutputSTRINGTerminalRuleCall_3_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getTransitionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"output",
                            		lv_output_4_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleTransition545); 

                	newLeafNode(otherlv_5, grammarAccess.getTransitionAccess().getAndKeyword_4());
                
            otherlv_6=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleTransition557); 

                	newLeafNode(otherlv_6, grammarAccess.getTransitionAccess().getGoKeyword_5());
                
            otherlv_7=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleTransition569); 

                	newLeafNode(otherlv_7, grammarAccess.getTransitionAccess().getToKeyword_6());
                
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:295:1: ( (otherlv_8= RULE_ID ) )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:296:1: (otherlv_8= RULE_ID )
            {
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:296:1: (otherlv_8= RULE_ID )
            // ../dk.itu.smdp.fsm.externalDSL/src-gen/dk/itu/smdp/fsm/parser/antlr/internal/InternalExternalDSL.g:297:3: otherlv_8= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getTransitionRule());
            	        }
                    
            otherlv_8=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleTransition589); 

            		newLeafNode(otherlv_8, grammarAccess.getTransitionAccess().getTargetStateCrossReference_7_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleFiniteStateMachine_in_entryRuleFiniteStateMachine75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFiniteStateMachine85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_ruleFiniteStateMachine122 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleFiniteStateMachine139 = new BitSet(new long[]{0x0000000000001002L});
        public static final BitSet FOLLOW_12_in_ruleFiniteStateMachine157 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_13_in_ruleFiniteStateMachine169 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleFiniteStateMachine189 = new BitSet(new long[]{0x000000000000C000L});
        public static final BitSet FOLLOW_ruleState_in_ruleFiniteStateMachine210 = new BitSet(new long[]{0x000000000000C000L});
        public static final BitSet FOLLOW_14_in_ruleFiniteStateMachine223 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleState_in_entryRuleState261 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleState271 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_ruleState308 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleState325 = new BitSet(new long[]{0x0000000000001002L});
        public static final BitSet FOLLOW_12_in_ruleState343 = new BitSet(new long[]{0x0000000000014000L});
        public static final BitSet FOLLOW_ruleTransition_in_ruleState364 = new BitSet(new long[]{0x0000000000014000L});
        public static final BitSet FOLLOW_14_in_ruleState377 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTransition_in_entryRuleTransition415 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTransition425 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_ruleTransition462 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleTransition474 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleTransition491 = new BitSet(new long[]{0x00000000000C0000L});
        public static final BitSet FOLLOW_18_in_ruleTransition509 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleTransition526 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_19_in_ruleTransition545 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_20_in_ruleTransition557 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleTransition569 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleTransition589 = new BitSet(new long[]{0x0000000000000002L});
    }


}