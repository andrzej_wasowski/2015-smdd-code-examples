package dk.itu.smdp.fsm.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import dk.itu.smdp.fsm.services.ExternalDSLGrammarAccess;
import fsm.FiniteStateMachine;
import fsm.FsmPackage;
import fsm.State;
import fsm.Transition;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;

@SuppressWarnings("all")
public abstract class AbstractExternalDSLSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private ExternalDSLGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == FsmPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case FsmPackage.FINITE_STATE_MACHINE:
				if(context == grammarAccess.getFiniteStateMachineRule()) {
					sequence_FiniteStateMachine(context, (FiniteStateMachine) semanticObject); 
					return; 
				}
				else break;
			case FsmPackage.STATE:
				if(context == grammarAccess.getStateRule()) {
					sequence_State(context, (State) semanticObject); 
					return; 
				}
				else break;
			case FsmPackage.TRANSITION:
				if(context == grammarAccess.getTransitionRule()) {
					sequence_Transition(context, (Transition) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (name=ID (initial=[State|ID] states+=State*)?)
	 */
	protected void sequence_FiniteStateMachine(EObject context, FiniteStateMachine semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID leavingTransitions+=Transition*)
	 */
	protected void sequence_State(EObject context, State semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (input=STRING output=STRING? target=[State|ID])
	 */
	protected void sequence_Transition(EObject context, Transition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
